
Logger.configure([level: :none])
Benchee.run(%{"auth"=> fn  ->

try do


  opts = [:binary, packet: 0, active: false, mode: :binary, header: 1]
  {:ok, socket} = :gen_tcp.connect('localhost', 11002, opts)
  %{socket: socket}

  {:ok, [header | data]} = :gen_tcp.recv(socket,0)
  handshake = Zen.Core.Packets.Parser.parse(header,data)
  {:handshake, data} =  handshake
  :ok =  :gen_tcp.send(socket, Zen.Core.Packets.Serializer.serialize(handshake))
  {:ok, [header | data]} = :gen_tcp.recv(socket,0)
  {:phase, {phase}} =  Zen.Core.Packets.Parser.parse(header,data)

  :ok =  :gen_tcp.send(socket, Zen.Core.Packets.Serializer.serialize({:login,"test","test",<<0>>,<<1>>}))
  {:ok, [header | data]} = :gen_tcp.recv(socket,0)
  #{:login, {phase}} =  Zen.Core.Packets.Parser.parse(header,data)
  :gen_tcp.close(socket)

rescue
x -> IO.inspect(x)

end

end }, save: %{path: "bench"}, parallel: 0,warmup: 5, time: 5, print: [fast_warning: false] )
