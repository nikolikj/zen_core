defmodule Zen.Core.World.MonsterRegeneration do
  alias Zen.Core.Utils.{Position, Point}
  alias Zen.Core.World.Entities
  alias Zen.Core.Prototypes.DataBank
  alias Zen.Core.World
  use Bitwise

  @doc """
  {#Reference<0.2825266082.2784231425.24133>,
   %{
     definition: {:group_group, 720, 829, 10, 10, 0, 0, "5s", 100, 1, 101},
     mobs: []
   }},
  """
  defp gen_mob_by_vnum(vnum) do
    Map.put(DataBank.get_mob(vnum), :name, "")
    |> Map.put(:state, 1 <<< 1)
  end

  defp spawn_mob(%{definition: {_type, x, y, _, _, _, _, _time, _, _, vnum}}) do
    char = gen_mob_by_vnum(vnum)

    mob = %{
      regen_id: 0,
      id: :rand.uniform(4_294_967_290),
      character: char,
      points: %{hp: char.max_hp, max_hp: char.max_hp},
      position: %Point{
        x: x * 100,
        y: y * 100
      },
      last_update: DateTime.utc_now()
    }

    mob
  end

  defp check_mob_group(nil, definition, now) do
    check_mob_group(spawn_mob(definition), definition, now)
  end

  defp check_mob_group(group, _definition, now) do
    update_mob(group, now)
  end

  def update_mob(%{position: position} = mob, now) do
    if(DateTime.diff(now, mob.last_update) >= 1) do
      %{
        mob
        | last_update: now,
          position: %Position{
            x: position.x + Enum.random(-200..200),
            y: position.y + Enum.random(-200..200)
          }
      }
    else
      mob
    end
  end

  def update(
        %{
          entities: entities,
          map: %{regen: regen_list, base_position: [base_x, base_y]}
        } = state
      ) do
    now = DateTime.utc_now()
    viewed_entites = Process.get(:viewed_entities)

    entities =
      Enum.reduce(regen_list, entities, fn {id, config}, acc ->
        spawned_mob =
          Enum.find(acc, fn {_vnum, ent} ->
            Entities.is_mob?(ent) and ent.regen_id == id
          end)

        mob =
          case spawned_mob do
            {uuid, ent} ->
              if uuid in viewed_entites do
                check_mob_group(ent, config, now)
              else
                ent
              end

            nil ->
              check_mob_group(nil, config, now)
          end

        mob = %{mob | regen_id: id}

        if mob.last_update == now do
          World.Map.inform_players(
            :move,
            entities,
            {:move, :move, 0, 0, mob.position.x + base_x, mob.position.y + base_y,
             now
             |> DateTime.to_unix(), mob.id}
          )
        end

        Map.put(acc, mob.id, mob)
      end)

    %{state | entities: entities}
  end
end
