defmodule Zen.Core.World.Entities do
  alias Zen.Core.Utils.{Position, Point, QuadTree, Rectangle}

  defp collect_positions(entities) do
    Enum.reduce(entities, [], fn {id, %{position: pos}}, acc ->
      acc ++ [%Point{x: pos.x, y: pos.y, extra: %{id: id}}]
    end)
  end

  def create_tree(map) do
    [map_x, map_y] = map.base_position
    [width, height] = map.map_size
    x_max = map_x + map.cell_scale * 128 * width
    y_max = map_y + map.cell_scale * 128 * height
    QuadTree.create(width: x_max - map_x, height: y_max - map_y, max_length: 32, max_depth: 3)
  end

  def by_id(id, entities) do
    Map.get(entities, id, nil)
  end

  @spec is_player?(%{character: %{type: atom()}}) :: boolean
  def is_player?(entity) do
    entity.character.type == :pc
  end

  @spec is_mob?(%{character: %{type: atom()}}) :: boolean
  def is_mob?(entity) do
    entity.character.type == :monster
  end

  def calculate_view(
        player,
        %{entities: entities, tree: tree, map: %{base_position: [base_x, base_y]}}
      ) do
    viewport =
      QuadTree.query(tree, %Rectangle{
        x: player.position.x - 100 * 64,
        y: player.position.y - 100 * 64,
        width: 200 * 64,
        height: 200 * 64
      })

    viewport = Enum.map(viewport, fn point -> point.extra.id end)

    cleaned_view =
      Enum.filter(player.view, fn old_uuid ->
        still_exists =
          Enum.any?(viewport, fn new_uuid ->
            old_uuid == new_uuid
          end)

        if still_exists or old_uuid == player.id do
          true
        else
          GenServer.cast(player.pid, {:delete_character, old_uuid})
          false
        end
      end)

    new_entities =
      Enum.filter(viewport, fn new_uuid ->
        not Enum.any?(cleaned_view, fn old_uuid ->
          new_uuid == old_uuid
        end)
      end)

    Enum.each(new_entities, fn entity ->
      if(entity != player.id) do
        ent = by_id(entity, entities)

        ent = %{
          ent
          | position: %{ent.position | x: ent.position.x + base_x, y: ent.position.y + base_y}
        }

        GenServer.cast(player.pid, {:add_character, ent})
      end
    end)

    %{player | view: viewport}
  end

  defp clear_viewed_entities() do
    Process.put(:viewed_entities, [])
  end

  defp remember_viewed_entities(entities) do
    ents =
      Enum.filter(entities, fn {_, ent} -> is_player?(ent) end)
      |> Enum.reduce([], fn {_, player}, acc ->
        Enum.reduce(player.view, acc, fn uuid, acc ->
          acc ++ [uuid]
        end)
      end)

    Process.put(:viewed_entities, ents)
  end

  def update_views(%{entities: entities} = state) do
    clear_viewed_entities()

    entities =
      Enum.map(entities, fn {id, entity} ->
        if is_player?(entity) do
          entity = calculate_view(entity, state)

          {id, entity}
        else
          {id, entity}
        end
      end)
      |> Enum.into(%{})

    remember_viewed_entities(entities)

    %{
      state
      | entities: entities
    }
  end

  def update_tree(%{tree: tree, entities: entities, last_positions: last_positions} = state) do
    positions = collect_positions(entities)

    new_positions = positions -- last_positions
    deleted_positions = last_positions -- positions

    new_tree =
      Enum.reduce(deleted_positions, tree, fn position, tree ->
        tree |> QuadTree.remove(position)
      end)

    new_tree =
      Enum.reduce(new_positions, new_tree, fn position, tree ->
        tree |> QuadTree.insert(position)
      end)

    state = %{state | tree: new_tree, last_positions: positions}

    state
  end
end
