defmodule Zen.Core.World.Combat do
  alias Zen.Core.World.Combat.{Monster, Player}
  alias Zen.Core.World.Entities

  def get_defence(character) do
    case character.character.type do
      :pc -> Player.get_defence(character)
      :monster -> Monster.get_defence(character)
    end
  end

  def calculate_damage(attacker, enemy) do
    case attacker.character.type do
      :pc -> Player.calculate_damage(attacker, enemy)
      :monster -> Monster.calculate_damage(attacker, enemy)
    end
  end

  def inform_damage({type, amount} = dmg, attacker, enemy) do
    if Map.has_key?(attacker, :pid) do
      GenServer.cast(attacker.pid, {:damage, enemy.id, type, amount})
      GenServer.cast(attacker.pid, {:target, enemy.id, enemy.points.hp / enemy.points.max_hp})
    end

    if Map.has_key?(enemy, :pid) do
      GenServer.cast(enemy.pid, {:damage, enemy.id, type, amount})
    end

    dmg
  end

  def is_alive?(who) do
    case who.character.type do
      :pc -> Player.is_alive?(who)
      :monster -> Monster.is_alive?(who)
    end
  end

  def apply_damage(dmg, enemy, state) do
    case enemy.character.type do
      :pc -> Player.apply_damage(dmg, enemy, state)
      :monster -> Monster.apply_damage(dmg, enemy, state)
    end
  end

  def attack(attacker, enemy, _type, state) do
    calculate_damage(attacker, enemy)
    |> inform_damage(attacker, enemy)
    |> apply_damage(enemy, state)
  end
end
