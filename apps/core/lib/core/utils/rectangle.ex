defmodule Zen.Core.Utils.Rectangle do
  use Bitwise
  defstruct [:width, :height, x: 0, y: 0]

  def create(x: x, y: y, width: width, height: height) do
    %__MODULE__{x: x, y: y, width: width, height: height}
  end

  def midpoint(rectangle) do
    %{
      x: rectangle.x + (rectangle.width >>> 1),
      y: rectangle.y + (rectangle.height >>> 1)
    }
  end

  def collides?(%{x: ax, y: ay, width: awidth, height: aheight}, %{
        x: bx,
        y: by,
        width: bwidth,
        height: bheight
      }) do
    ax < bx + bwidth and ax + awidth > bx and ay < by + bheight and aheight + ay > by
  end
end
