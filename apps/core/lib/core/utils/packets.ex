defmodule Zen.Core.Utils.Packets do
  alias Zen.Core.Utils.Packets.Serializer
  require Logger

  @spec send_packet(
          %{socket: any, transport: atom},
          {:ping}
          | {atom, any}
          | {:login_success, integer, <<_::8, _::_*1>>}
          | {:mark_index, integer, any}
          | {:chat, atom | integer, integer, integer, binary}
          | {:login, binary, binary, binary, <<_::8, _::_*1>>}
        ) :: %{socket: any, transport: atom}
  def send_packet(%{transport: transport, socket: socket} = state, data) when is_tuple(data) do
    # packet_type = elem(data, 0)
    # Logger.debug("Sending Packet #{packet_type}")
    transport.send(socket, Serializer.serialize(data))
    state
  end

  @spec send_packets(%{socket: :ranch_transport.socket(), transport: atom()}, [{atom, any()}]) ::
          any
  def send_packets(state, data) when is_list(data) do
    Enum.reduce(data, state, fn packet, acc -> send_packet(acc, packet) end)
  end
end
