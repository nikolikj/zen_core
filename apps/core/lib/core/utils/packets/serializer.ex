defmodule Zen.Core.Utils.Packets.Serializer do
  alias Zen.Core.World.{Stats, ServerAddress, SimplePlayer}

  @spec serialize_stats(%Stats{}) :: <<_::32>>
  defp serialize_stats(stats) do
    <<
      stats.strength::little-integer-size(8),
      stats.health::little-integer-size(8),
      stats.dexterity::little-integer-size(8),
      stats.intelligence::little-integer-size(8)
    >>
  end

  @spec serialize_address(%ServerAddress{}) :: binary()
  defp serialize_address(address) do
    {:ok, ip} =
      address.ip
      |> String.to_charlist()
      |> :inet.parse_address()

    ip_binary =
      Tuple.to_list(ip)
      |> Enum.map(fn x -> <<x::little-integer-size(8)>> end)
      |> :erlang.list_to_binary()

    <<ip_binary::binary(), address.port::little-integer-size(16)>>
  end

  @guild_id <<
    0::little-integer-size(32),
    0::little-integer-size(32),
    0::little-integer-size(32),
    0::little-integer-size(32)
  >>
  defp guild_name do
    name = String.pad_trailing("", 13, <<0>>)
    name <> name <> name <> name
  end

  @spec serialize_player(%SimplePlayer{}) :: binary()
  defp serialize_player(player) do
    player_name = String.pad_trailing(player.character.name, 25, <<0>>)
    stats = serialize_stats(player.character.stats)
    stats_size = byte_size(stats)
    address_binary = serialize_address(player.server_address)
    address_size = byte_size(address_binary)

    <<
      player.id::little-integer-size(32),
      player_name::binary-size(25),
      player.character.vnum::little-integer-size(8),
      player.character.level::little-integer-size(8),
      player.character.minutes::little-integer-size(32),
      stats::binary-size(stats_size),
      player.character.armor::little-integer-size(16),
      0::little-integer-size(8),
      player.character.hair::little-integer-size(16),
      0::little-integer-size(32),
      player.position.x::little-signed-integer-size(32)-signed,
      player.position.y::little-signed-integer-size(32)-signed,
      address_binary::binary-size(address_size),
      player.character.skill_group::little-integer-size(8)
    >>
  end

  @spec serialize_main_character(%SimplePlayer{}) :: binary()
  defp serialize_main_character(player) do
    player_name = String.pad_trailing(player.character.name, 25, <<0>>)

    <<
      player.id::little-integer-size(32),
      player.character.vnum::little-integer-size(16),
      player_name::binary-size(25),
      player.position.x::little-signed-integer-size(32),
      player.position.y::little-signed-integer-size(32),
      player.position.z::little-signed-integer-size(32),
      player.character.empire::little-integer-size(8),
      player.character.skill_group::little-integer-size(8)
    >>
  end

  @spec serialize(
          {:ping}
          | {atom, any}
          | {:login_success, integer, <<_::8, _::_*1>>}
          | {:mark_index, integer, any}
          | {:chat, atom | integer, integer, integer, binary}
          | {:login, binary, binary, binary, <<_::8, _::_*1>>}
          | {:main_character, %SimplePlayer{}}
          | {:move,
             {:move, non_neg_integer(), non_neg_integer(), non_neg_integer(), non_neg_integer(),
              non_neg_integer(), non_neg_integer(), non_neg_integer()}}
          | {:handshake, {non_neg_integer(), non_neg_integer(), non_neg_integer()}}
          | {:walk_mode, {non_neg_integer(), non_neg_integer()}}
          | {:phase, {atom() | non_neg_integer()}}
        ) :: bitstring()
  def serialize({:main_character, character}) do
    character = serialize_main_character(character)

    <<113, character::binary()>>
  end

  def serialize({:move, {:move, func, arg, rot, x, y, time, uuid}}) do
    func = Zen.Core.Enums.get_move(func)

    <<3, func::little-integer-size(8), arg::little-integer-size(8), rot::little-integer-size(8),
      uuid::little-integer-size(32), x::little-integer-size(32), y::little-integer-size(32),
      time::little-integer-size(32), 0::little-integer-size(32)>>
  end

  def serialize({:damage, {uuid, flag, damage}}) do
    flag = Zen.Core.Enums.get_damage_flag(flag)

    <<135, uuid::little-integer-size(32), flag::little-integer-size(8),
      damage::signed-little-integer-size(32)>>
  end

  def serialize({:target, {uuid, hp}}) do
    hp = Kernel.trunc(hp * 100)
    <<63, uuid::little-integer-size(32), hp::little-integer-size(8)>>
  end

  def serialize({:handshake, {handshake, time, delta}}) do
    <<
      255,
      handshake::little-integer-size(32),
      time::little-integer-size(32),
      delta::little-integer-size(32)
    >>
  end

  def serialize({:walk_mode, {uuid, mode}}) do
    <<111, uuid::little-integer-size(32), mode::little-integer-size(8)>>
  end

  def serialize({:phase, phase}) when is_atom(phase) do
    phase = Zen.Core.Enums.get_phase(phase)

    serialize({
      :phase,
      phase
    })
  end

  def serialize({:send_points, %{points: points}}) when is_map(points) do
    points =
      Map.to_list(points)
      |> Enum.map(fn {type, value} ->
        {Zen.Core.Enums.get_point(type), value}
      end)
      |> Enum.into(%{})

    list =
      Enum.map(0..255, fn i ->
        point = Map.get(points, i, 0)
        <<point::little-integer-size(32)>>
      end)

    {value, list} = List.pop_at(list, 0)

    encoded =
      list
      |> Enum.reduce(value, fn a, acc ->
        acc <> a
      end)

    <<16, encoded::binary()>>
  end

  def serialize({:set_time, time}) do
    <<106, time::little-integer-size(32)>>
  end

  def serialize({:set_channel, channel}) do
    <<121, channel::little-integer-size(8)>>
  end

  def serialize({:phase, phase}) when is_integer(phase) do
    <<253, phase::little-integer-size(8)>>
  end

  def serialize({:chat, type, vid, empire, message}) do
    length = 9 + byte_size(message)
    type = Zen.Core.Enums.get_chat_type(type)

    <<
      4,
      length::little-integer-size(16),
      type::little-integer-size(8),
      vid::little-integer-size(32),
      empire::little-integer-size(8),
      message::binary()
    >>
  end

  def serialize({:delete_character, %SimplePlayer{} = character}) do
    <<
      2,
      character.id::little-integer-size(32)
    >>
  end

  def serialize({:delete_character, character}) when is_integer(character) do
    <<
      2,
      character::little-integer-size(32)
    >>
  end

  def serialize({:send_character, %{position: pos, character: char} = entitiy}) do
    type = Zen.Core.Enums.get_char_type(char.type)

    angle = Map.get(pos, :angle, 0)
    z = Map.get(pos, :z, 0)

    <<
      1,
      entitiy.id::little-integer-size(32),
      angle::little-float-size(32),
      pos.x::little-signed-integer-size(32),
      pos.y::little-signed-integer-size(32),
      z::little-signed-integer-size(32),
      type::little-integer-size(8),
      char.vnum::little-integer-size(16),
      255::little-integer-size(8),
      255::little-integer-size(8),
      char.state::little-integer-size(8),
      0::little-integer-size(32),
      0::little-integer-size(32)
    >>
  end

  def serialize({:send_character_additional, %SimplePlayer{} = player}) do
    character_name = String.pad_trailing(player.character.name, 25, <<0>>)

    <<
      136,
      player.id::little-integer-size(32),
      character_name::binary-size(25),
      player.character.armor::little-integer-size(16),
      player.character.weapon::little-integer-size(16),
      player.character.head::little-integer-size(16),
      player.character.hair::little-integer-size(16),
      player.character.empire::little-integer-size(8),
      0::little-integer-size(32),
      player.character.level::little-integer-size(32),
      10000::little-integer-size(16),
      0::little-integer-size(8),
      0::little-integer-size(32)
    >>
  end

  def serialize({:login_failed, status}) do
    status = String.pad_trailing(status, 9, <<0>>)

    <<7, status::binary-size(9)>>
  end

  def serialize({:login, login, password, key, sequence}) do
    login = String.pad_trailing(login, 31, <<0>>)
    password = String.pad_trailing(password, 17, <<0>>)
    key = String.pad_trailing(key, 16, <<0>>)

    <<
      111,
      login::binary-size(31),
      password::binary-size(17),
      key::binary-size(16),
      sequence::binary-size(1)
    >>
  end

  def serialize({:login_success, key, result}) do
    <<150, key::little-integer-size(32), result::binary-size(1)>>
  end

  def serialize({:channel_status, channel_statuses}) when is_list(channel_statuses) do
    data =
      Enum.reduce(
        channel_statuses,
        <<>>,
        fn x, acc ->
          acc <> serialize_channel_status(x)
        end
      )

    size = length(channel_statuses)
    <<210, size::little-integer-size(32)>> <> data
  end

  @spec serialize({atom(), list(%SimplePlayer{})}) :: binary()
  def serialize({:login_success_data, players}) do
    # vid, vnum, name, pos, skillgroup
    player_data =
      players
      |> Enum.reduce(nil, fn player, acc ->
        case acc do
          nil -> serialize_player(player)
          x -> x <> serialize_player(player)
        end
      end)

    <<32>> <>
      player_data <>
      @guild_id <>
      guild_name() <> <<5::little-integer-size(32), 2::little-integer-size(32)>>
  end

  def serialize({:mark_index, data}) when is_list(data) do
    Enum.reduce(
      data,
      <<>>,
      fn {guild_id, mark_id}, acc ->
        acc <>
          <<guild_id::little-integer-size(32), mark_id::little-integer-size(32)>>
      end
    )
  end

  def serialize({:empire, empire}) do
    <<90, empire::little-integer-size(8)>>
  end

  def serialize({:mark_index, count, data}) do
    # 2x32bit* count
    size = 4 * 2 * count

    <<102, size::little-integer-size(32), count::little-integer-size(32)>> <>
      serialize({:mark_index, data})
  end

  def serialize({:ping}) do
    <<44>>
  end

  @spec serialize_channel_status({integer, integer}) :: <<_::24>>
  def serialize_channel_status({port, status}) do
    <<port::little-integer-size(16), status::little-integer-size(8)>>
  end
end
