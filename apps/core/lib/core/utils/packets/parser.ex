defmodule Zen.Core.Utils.Packets.Parser do
  require Logger
  @type packet_data :: {atom(), tuple()}
  @type unprocessed_data :: binary() | <<>>
  @type parsed_packet :: {packet_data(), unprocessed_data()}

  @spec parse_all(binary()) :: list(parsed_packet())

  def parse_all(<<header, data::binary()>>, acc \\ []) do
    {packet, rest} = parse(header, data)

    case rest do
      "" ->
        acc ++ [packet]

      x ->
        parse_all(x, acc ++ [packet])
    end
  end

  @spec parse(non_neg_integer(), binary()) :: parsed_packet
  def parse(8, <<key::64-integer-unsigned, index::64-integer-unsigned, rest::binary()>>) do
    {{:state_check, key, index}, rest}
  end

  def parse(
        2,
        <<type::little-8-integer-unsigned, uuid::little-32-integer-unsigned,
          _bullshitCRCA::8-integer-unsigned, _bullshitCRCB::8-integer-unsigned,
          _seq::8-integer-unsigned, rest::binary()>>
      ) do
    {{:attack, {type, uuid}}, rest}
  end

  def parse(
        0xFF,
        <<
          handshake::little-32-integer-unsigned,
          time::little-32-integer-unsigned,
          delta::little-32-integer-unsigned,
          rest::binary()
        >>
      ) do
    {{:handshake, {handshake, time, delta}}, rest}
  end

  def parse(0xFD, <<phase::integer-unsigned-size(8), rest::binary()>>) do
    {{:phase, {phase}}, rest}
  end

  def parse(
        111,
        <<
          login::binary-size(31),
          passwd::binary-size(17),
          key::binary-size(16),
          sequence::binary-size(1),
          rest::binary()
        >>
      ) do
    [login, _crap] = String.chunk(login, :printable)
    [passwd, _crap] = String.chunk(passwd, :printable)
    {{:login, {login, passwd, key, sequence}}, rest}
  end

  def parse(
        109,
        <<
          login::binary-size(31),
          login_key::unsigned-integer-size(32),
          key::binary-size(16),
          sequence::binary-size(1),
          rest::binary()
        >>
      ) do
    [login, _crap] = String.chunk(login, :printable)
    {{:login2, {login, login_key, key, sequence}}, rest}
  end

  def parse(150, <<key::little-integer-size(32), result::binary-size(1), rest::binary()>>) do
    {{:login_success, {key, result}}, rest}
  end

  def parse(
        7,
        <<
          func::little-integer-size(8),
          arg::little-integer-size(8),
          rot::little-integer-size(8),
          x::little-integer-size(32),
          y::little-integer-size(32),
          time::little-integer-size(32),
          _seq::binary-size(1),
          rest::binary
        >>
      ) do
    {{:move, Zen.Core.Enums.get_move(func), arg, rot, x, y, time}, rest}
  end

  def parse(
        61,
        <<
          vid::little-integer-size(32),
          _seq::binary-size(1),
          rest::binary
        >>
      ) do
    {{:command_target, vid}, rest}
  end

  def parse(
        26,
        <<
          vid::little-integer-size(32),
          _seq::binary-size(1),
          rest::binary
        >>
      ) do
    {{:on_click, vid}, rest}
  end

  def parse(
        3,
        <<
          length::little-integer-size(16),
          type::little-integer-size(8),
          rest::binary()
        >>
      ) do
    length = length - 4
    type = Zen.Core.Enums.get_chat_type(type)
    <<message::binary-size(length), _seq::binary-size(1), rest::binary>> = rest
    [message, _crap] = String.chunk(message, :printable)
    {{:chat, type, message}, rest}
  end

  def parse(6, <<player_index::little-integer-size(8), _sequence::binary-size(1), rest::binary>>) do
    {{:command_character_select, player_index}, rest}
  end

  def parse(
        241,
        <<
          filename::binary-size(33),
          timestamp::binary-size(33),
          _sequence::binary-size(1),
          rest::binary
        >>
      ) do
    {{:client_version, {filename, timestamp}}, rest}
  end

  def parse(206, <<"", rest::binary()>>) do
    {{:request_channel_status}, rest}
  end

  def parse(10, <<"", _seq::binary-size(1), rest::binary()>>) do
    {{:enter_game}, rest}
  end

  @crcsize 4 * 80
  def parse(
        101,
        <<
          imgIDX::unsigned-integer-size(8),
          crclist::binary-size(@crcsize)-unit(8),
          rest::binary()
        >>
      ) do
    {{:mark_crclist, imgIDX, crclist}, rest}
  end

  def parse(254, <<"", rest::binary()>>) do
    {{:pong}, rest}
  end

  def parse(90, <<empire::unsigned-integer-size(8), sequence::binary-size(1), rest::binary()>>) do
    {{:empire_select, {empire, sequence}}, rest}
  end

  def parse(104, <<"", rest::binary()>>) do
    {{:mark_index_request}, rest}
  end

  def parse(14, <<"", rest::binary()>>) do
    {{:mark_index_request}, rest}
  end

  # def parse(111,_x) do
  # {:login3, {"login","passwd","key"}}
  #  |>IO.inspect
  # end
end
