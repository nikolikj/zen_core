defmodule Zen.Core.Utils.Point do
  use Bitwise
  defdelegate fetch(term, key), to: Map
  defdelegate get(term, key, default), to: Map
  defdelegate get_and_update(term, key, fun), to: Map
  defstruct x: 0, y: 0, extra: %{}

  def create(x: x, y: y, extra: extra) do
    %__MODULE__{x: x, y: y, extra: extra}
  end

  def collides?(%{x: ax, y: ay}, %{x: bx, y: by, width: bwidth, height: bheight}) do
    ax < bx + bwidth and ax > bx and ay < by + bheight and ay > by
  end

  def distance(p1, p2), do: :math.sqrt(distance_squared(p1, p2))

  defp distance_squared(%{x: ax, y: ay}, %{x: bx, y: by}) do
    dx = ax - bx
    dy = ay - by

    dx * dx + dy * dy
  end
end
