defmodule Zen.Core.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def launch() do
    if Enum.empty?(Node.list()) do
      LocalCluster.start_nodes("others", 1)
    end
  end

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Zen.Core.Worker.start_link(arg)
      # {Zen.Core.Worker, arg}
      {Horde.Registry, [name: Zen.Core.GameRegistry, keys: :unique, members: :auto]},
      {Zen.Core.Prototypes.DataBank, ""},
      {Zen.Core.Prototypes.Maps, ""},
      {
        Horde.DynamicSupervisor,
        [name: Zen.Core.DistributedSupervisor, strategy: :one_for_one, members: :auto]
      }
    ]

    if unquote(Mix.env()) == :test do
      LocalCluster.start()
      launch()
    end

    if unquote(Mix.env()) == :dev do
      :observer.start()
    end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Zen.Core.Supervisor, max_restarts: 50]
    sup = Supervisor.start_link(children, opts)

    fn ->
      Process.sleep(0)
      #  Horde.DynamicSupervisor.start_child(Zen.Core.DistributedSupervisor, Zen.Core.World)
      <<i1::unsigned-integer-32, i2::unsigned-integer-32, i3::unsigned-integer-32>> =
        :crypto.strong_rand_bytes(12)

      :rand.seed(:exsplus, {i1, i2, i3})

      Horde.DynamicSupervisor.start_child(
        Zen.Core.DistributedSupervisor,
        {Zen.Core.LoginListener, [name: Zen.Core.LoginListener, port: 11002]}
      )

      Horde.DynamicSupervisor.start_child(
        Zen.Core.DistributedSupervisor,
        {Zen.Core.WorldListener, [name: Zen.Core.WorldListener, port: 13001]}
      )

      Zen.Core.Prototypes.Maps.import_data()
      |> Enum.map(fn {map_id, map} ->

        Horde.DynamicSupervisor.start_child(
          Zen.Core.DistributedSupervisor,
          {Zen.Core.World.Map, [map_id: map_id]}
        )
      end)
    end
    |> Task.start()

    sup
  end
end
