defmodule Zen.Core.Login.ConnectionHandler do
  use GenServer
  require Logger
  alias Zen.Core.Utils.Packets.{Serializer, Parser}
  alias Zen.Core.Utils.Packets

  @behaviour :ranch_protocol

  def start_link(ref, socket, transport) do
    pid = :proc_lib.spawn_link(__MODULE__, :init, [ref, socket, transport])
    {:ok, pid}
  end

  def init(init_arg) do
    {:ok, init_arg}
  end

  def init(ref, transport, _opts) do
    {:ok, socket} = :ranch.handshake(ref)

    # {:header, 1}
    :ok = transport.setopts(socket, [{:active, true}, {:packet, 0}, {:mode, :binary}])

    init_state = %{
      socket: socket,
      transport: transport,
      login_state: :new,
      user: %{}
    }

    now = System.os_time(:second)

    Packets.send_packet(init_state, {:handshake, {now, now, 0}})

    :gen_server.enter_loop(
      __MODULE__,
      [],
      init_state
    )
  end

  defp handle(
         {:handshake, {_handshake, time, delta} = hs},
         state
       ) do
    now = System.os_time(:second)
    time = time + delta
    diff = now - time

    if diff <= 50 do
      Packets.send_packet(state, {:phase, 10})
      %{state | login_state: :select}
    else
      Packets.send_packet(state, {:handshake, {:rand.uniform(60000), now, (now - time) / 2}})
      %{state | login_state: :handshake}
    end
  end

  defp handle(
         {:login, {username, password, _key, _seq}},
         %{socket: socket} = state
       ) do
    Logger.info("Handle Login")

    case Zen.Core.Login.Accounts.authenticate(username, password) do
      {_username, _password} = user ->
        Packets.send_packet(state, {:login_success, 1, <<1>>})

        %{state | login_state: :authenticated, user: user}

      false ->
        Packets.send_packets(state, [{:login_failed, "WRONGPWD"}, {:phase, 0}])
        Process.send_after(self(), {:tcp_closed, socket}, 1)
        %{state | login_state: :failed}
    end
  end

  def handle_info(
        {:tcp, _socket, data},
        state
      ) do
    Logger.info("Handle packet")

    state =
      Parser.parse_all(data)
      |> Enum.reduce(state, &handle/2)

    {:noreply, state}
  end

  def handle_info({:tcp_closed, socket}, state = %{socket: socket, transport: transport}) do
    transport.close(socket)
    {:stop, :normal, state}
  end

  def handle_info(unknown, state) do
    Logger.error("UNKNOWN PACKET #{inspect(unknown)}")
    {:noreply, state}
  end
end
