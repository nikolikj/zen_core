defmodule Zen.Core.Utils do
  def point_in_square(min_x, min_y, max_x, max_y, point_x, point_y) do
    point_x >= min_x and point_x <= max_x and point_y >= min_y and point_y <= max_y
  end
end
