defmodule Zen.Core.Prototypes.DataBank do
  alias Zen.Core.Utils.StructuredTextReader
  use Agent
  NimbleCSV.define(__MODULE__.Parser, separator: "\t", escape: "\"")

  @moduledoc """
  Mob and Item Prototypes DataBank
  @TODO: Should be an ETS Table, not an Agent, and be CLeaned up...
  """
  def start_link(_x \\ "") do
    Agent.start_link(
      fn ->
        init_ets()

        import_data()
        |> insert_into_ets()
      end,
      name: __MODULE__
    )
  end

  def get_mob(vnum) do
    [{_, mob}] = :ets.lookup(:mob_proto, vnum)
    mob
  end

  def get_item(vnum) do
    [{_, item}] = :ets.lookup(:item_proto, vnum)
    item
  end

  defp init_ets() do
    :ets.new(:mob_proto, [:set, :named_table])
    :ets.new(:item_proto, [:set, :named_table])
    :ets.new(:dragon_soul_table, [:set, :named_table])
    :ets.new(:group_group, [:set, :named_table])
    :ets.new(:group, [:set, :named_table])
    :ets.new(:mob_drop_item, [:set, :named_table])
    :ets.new(:special_item_group, [:set, :named_table])
  end

  defp insert_file(file, data) when file in [:item_proto, :mob_proto] do
    Enum.each(data, fn entry ->
      :ets.insert_new(file, {entry.vnum, entry})
    end)
  end

  defp insert_file(file, data) do
    Enum.each(data, fn {key, value} ->
      :ets.insert_new(file, {key, value})
    end)
  end

  defp insert_into_ets(data) do
    Enum.each(data, fn {file_type, data} ->
      case file_type do
        "item_proto.txt" -> insert_file(:item_proto, data)
        "mob_proto.txt" -> insert_file(:mob_proto, data)
        "dragon_soul_table.txt" -> insert_file(:dragon_soul_table, data)
        "group_group.txt" -> insert_file(:group_group, data)
        "group.txt" -> insert_file(:group, data)
        "mob_drop_item.txt" -> insert_file(:mob_drop_item, data)
        "special_item_group.txt" -> insert_file(:special_item_group, data)
      end
    end)
  end

  def value do
    Agent.get(__MODULE__, & &1)
  end

  def get_by_field(file, field, value) do
    Agent.get(
      __MODULE__,
      fn state ->
        {_file, data} =
          state
          |> Enum.find(fn {f, _data} ->
            f == file
          end)

        Enum.find(data, fn item -> Map.get(item, field) == value end)
      end
    )
  end

  defp format(atoms, strings, floats, numbers, key, value) do
    try do
      cond do
        key in atoms ->
          String.to_atom(Macro.underscore(value))

        key in strings ->
          value

        key in floats and value == "" ->
          0.0

        key in floats ->
          {float, _rest} = Float.parse(value)
          float

        key in numbers and value == "" ->
          0

        key in numbers ->
          String.to_integer(value)

        true ->
          IO.inspect({key, value})
          raise "FUCK"
          value
      end
    rescue
      x in [RuntimeError] -> raise x
      _x -> value
    end
  end

  defp format_mob(key, value) do
    strings = ["Name", "Folder"]
    atoms = ["Rank", "Type", "BattleType", "AiFlags", "RaceFlags", "ImmuneFlags", "Size"]
    floats = ["RegenPercent", "AggressiveHpPct", "AttackRange", "DamMultiply"]

    numbers = [
      "Vnum",
      "Level",
      "MountCapacity",
      "Empire",
      "OnClick",
      "St",
      "Dx",
      "Ht",
      "Iq",
      "MinDamage",
      "MaxDamage",
      "MaxHp",
      "RegenCycle",
      "MinGold",
      "MaxGold",
      "Exp",
      "Def",
      "AttackSpeed",
      "MoveSpeed",
      "AggressiveSight",
      "DropItemGroup",
      "ResurrectionVnum",
      "EnchantCurse",
      "EnchantSlow",
      "EnchantPoison",
      "EnchantStun",
      "EnchantCritical",
      "EnchantPenetrate",
      "ResistSword",
      "ResistTwoHanded",
      "ResistDagger",
      "ResistBell",
      "ResistFan",
      "ResistBow",
      "ResistFire",
      "ResistElect",
      "ResistMagic",
      "ResistWind",
      "ResistPoison",
      "SummonVnum",
      "DrainSp",
      "MobColor",
      "PolymorphItem",
      "SkillLevel0",
      "SkillVnum0",
      "SkillLevel1",
      "SkillVnum1",
      "SkillLevel2",
      "SkillVnum2",
      "SkillLevel3",
      "SkillVnum3",
      "SkillLevel4",
      "SkillVnum4",
      "SpBerserk",
      "SpStoneskin",
      "SpGodspeed",
      "SpDeathblow",
      "SpRevive"
    ]

    format(atoms, strings, floats, numbers, key, value)
  end

  defp format_item(key, value) do
    strings = ["Name", "Folder"]

    atoms = [
      "Type",
      "SubType",
      "Size",
      "AntiFlags",
      "Flags",
      "WearFlags",
      "ImmuneFlags",
      "LimitType0",
      "LimitType1",
      "ApplyType0",
      "ApplyType1",
      "ApplyType2",
      "AddonType",
      "Value0"
    ]

    floats = ["AlterToMagicItemPercent", "GainSocketPercent"]

    numbers = [
      "Vnum",
      "Level",
      "Gold",
      "ShopBuyPrice",
      "RefinedVnum",
      "RefineSet",
      "LimitValue0",
      "LimitValue1",
      "ApplyValue0",
      "ApplyValue1",
      "ApplyValue2",
      "Value0",
      "Value1",
      "Value2",
      "Value3",
      "Value4",
      "Value5",
      "Specular"
    ]

    format(atoms, strings, floats, numbers, key, value)
  end

  defp parse_row(_x, row, "") do
    row
  end

  defp parse_row("mob_proto.txt", row, headers) do
    Enum.zip(headers, row)
    |> Enum.map(fn {key, val} ->
      {String.to_atom(Macro.underscore(key)), format_mob(key, val)}
    end)
    |> Enum.into(%{})
  end

  defp parse_row("item_proto.txt", row, headers) do
    Enum.zip(headers, row)
    |> Enum.map(fn {key, val} ->
      {String.to_atom(Macro.underscore(key)), format_item(key, val)}
    end)
    |> Enum.into(%{})
  end

  defp parse_rows(file_name, row, acc) do
    parsed = parse_row(file_name, row, acc)
    # if acc empty its first row(CSV HEADERS, they should not appear in the Result)
    if acc == "" do
      {[nil], parsed}
    else
      {[parsed], acc}
    end
  end

  defp import_proto do
    path = "data/conf"
    {:ok, files} = File.ls(path)

    data =
      Enum.filter(files, fn x -> String.contains?(x, "proto") end)
      |> Enum.map(fn file_name ->
        data =
          File.stream!(path <> "/#{file_name}", read_ahead: 1000)
          |> __MODULE__.Parser.parse_stream(skip_headers: false)
          |> Stream.transform("", fn row, acc -> parse_rows(file_name, row, acc) end)
          |> Stream.filter(fn x -> not is_nil(x) end)
          |> Enum.to_list()

        {
          file_name,
          data
        }
      end)

    data
  end

  defp fix_data(data, "group.txt") do
    data
    |> Enum.reduce(%{}, fn {group, data} = entry, acc ->
      data = prep_data(entry)

      props =
        Enum.map(data.props, fn data ->
          key = List.first(data)

          key =
            if(is_binary(key) and String.downcase(key) in ["vnum", "leader"]) do
              String.to_atom(Macro.underscore(key))
            else
              key
            end

          {key, List.last(data)}
        end)
        |> Enum.into(%{})

      Map.put_new(acc, props.vnum, props)
    end)
  end

  defp prep_data({group, data}) do
    if is_list(group) do
      put_in(data.props, data.props ++ [group])
    else
      data
    end
  end

  defp fix_data(data, "group_group.txt") do
    data
    |> Enum.reduce(%{}, fn {group, data} = entry, acc ->
      data = prep_data(entry)

      props =
        Enum.map(data.props, fn data ->
          key = List.first(data)

          key =
            if(is_binary(key) and String.downcase(key) in ["vnum", "leader"]) do
              String.to_atom(Macro.underscore(key))
            else
              key
            end

          {key, List.last(data)}
        end)
        |> Enum.into(%{})

      Map.put_new(acc, props.vnum, props)
    end)
  end

  defp fix_data(data, file_name), do: data

  defp import_common do
    path = "data/locale/singapore"

    files = [
      "dragon_soul_table.txt",
      "group_group.txt",
      "group.txt",
      "mob_drop_item.txt",
      "special_item_group.txt"
    ]

    data =
      Enum.filter(files, fn file_name -> File.exists?("#{path}/#{file_name}") end)
      |> Enum.map(fn file_name ->
        {:ok, %{"ROOT" => data}} = StructuredTextReader.parse_file("#{path}/#{file_name}")

        {
          file_name,
          data |> fix_data(file_name)
        }
      end)

    data
  end

  def import_data do
    import_proto() ++ import_common()
  end
end
